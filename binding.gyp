 {
    'targets': [
      {
        'target_name': 'qrencoder',
        
        'sources': ['src/QRencoder.cpp',
        	'src/module.cpp',
        	'src/lib/bitstream.c',
        	'src/lib/mask.c',
        	'src/lib/mmask.c',
        	'src/lib/mqrspec.c',
        	'src/lib/qrencode.c',
        	'src/lib/qrinput.c',
        	'src/lib/qrspec.c',
        	'src/lib/rscode.c',
        	'src/lib/split.c'
        ],
        
        'include_dirs': [
          'src/lib',
        ],
        
        'defines': [
              'HAVE_CONFIG_H',
            ],
      },
    ],
  }
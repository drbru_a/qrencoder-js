var qrencoder = require('../build/Release/qrencoder').qr,
  Png = require('png').Png,
  util = require('util');

function Color(text) {
  var cstr = text.substr(1, text.length-1);
  this.r = parseInt('0x'+cstr.substr(0, 2));
  this.g = parseInt('0x'+cstr.substr(2, 2));
  this.b = parseInt('0x'+cstr.substr(4, 2));

  console.log(text+' '+cstr+' '+this.r+' '+this.g+' '+this.b);
};

exports.img_png = function(text, params, callback) {
  console.log('img_png');
  var qr = new qrencoder(),
    color_bg = params.bg ? new Color(params.bg) : new Color('#FFFFFF'),
    color_fg = params.fg ? new Color(params.fg) : new Color('#000000'),
    boxsize = params.boxsize || 8,
    margin = params.margin || 2,
    eclevel = params.eclevel || 'H';

  console.log(text+' '+util.inspect(params));

  function drawPixel(buf, x, y, w, color) {
    var p = (y*w+x)*3;
    buf.writeUInt8(color.r, p);
    buf.writeUInt8(color.g, p+1);
    buf.writeUInt8(color.b, p+2);
  };

  function drawRect(buf, x, y, width, height, symwidth, psize, color) {
    var wp = width*psize,
      hp = height*psize,
      dp = (symwidth-width)*psize*3,
      p = (y*symwidth*psize+x)*psize*3;

    for(var j=0; j<hp; j+=1) {
      for(var i=0; i<wp; i+=1) {
        buf.writeUInt8(color.r, p);
        buf.writeUInt8(color.g, p+1);
        buf.writeUInt8(color.b, p+2);
        p+=3;
      }
      p+=dp;
    }
  };

  qr.encode(text.toString(), {ec: eclevel}, function(err, arr) {
    if(!err) {
      var width = arr.length,
        symwidth = width + 2*margin,
        realwidth = symwidth*boxsize,
        buffer = new Buffer(realwidth*realwidth*9),
        x = 0, y = 0, pen = 0, x0 = 0;

      drawRect(buffer, 0, 0, symwidth, symwidth, symwidth, boxsize, color_bg);

      for(y=0; y<width; y+=1) {
        pen = 0;
        x0 = 0;
        for(x=0; x<width; x+=1) {
          if(!pen) {
            pen = arr[y][x];
            x0 = x;
          } else {
            if(arr[y][x]===0) {
              drawRect(buffer, x0+margin, y+margin, x-x0, 1, symwidth, boxsize, color_fg);
              pen = 0;
            }
          }
        }
        if(pen) {
          drawRect(buffer, x0+margin, y+margin, width-x0, 1, symwidth, boxsize, color_fg);
        }
      }

      var png = new Png(buffer, realwidth, realwidth, 'rgb');
      png.encode(function(data) {
        callback(null, data);
      });
    } else {
      callback(true);
    }
  });
};

exports.img_svg = function(text, params, callback) {
  var color_bg = params.bg ? params.bg : '#FFFFFF',
    color_fg = params.fg ? params.fg : '#000000',
    boxsize = params.boxsize || 8,
    margin = params.margin || 2,
    eclevel = params.eclevel || 'H',
    arc = params.arc || false;

  console.log(params.arc);
  console.log('img_svg');

  function svgGenerator(arr, bg_color, fg_color) {
    var out = '',
      width = arr.length,
      INCHES_PER_METER = 100.0/2.54,
      dpi = 72,
      scale = dpi * INCHES_PER_METER / 100.0,
      symwidth = width + margin * 2,
      realwidth = symwidth * boxsize,
      x = 0, y = 0, pen = 0, x0 = 0, dp = 0, dw = 0;

    function genRect(_x, _y, _w, _color) {
      return util.format('\t\t\t<rect x="%d" y="%d" width="%d" height="1.02" stroke-width="0" fill="%s" />\n', _x, _y, _w, _color);
    };

    if(arc) {
      //dp = -5.0;
      dp = 0.0;
      dw = 5.0;
    }

    out = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n';
    out += util.format('<svg width="%scm" height="%scm" viewBox="%d %d %d %d" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">\n',
      (realwidth / scale).toFixed(2), (realwidth / scale).toFixed(2), dp, dp, symwidth+dw, symwidth+dw
    );
    out += '\t<g id="QRcode">\n';
    if(arc) {
      out += util.format('\t\t<rect x="%d" y="%d" width="%d" height="%d" fill="%s"/>\n', dp, dp, symwidth+dw, symwidth+dw, bg_color);
      out += util.format('\t\t<circle cx="%d" cy="%d" r="4" stroke="%s" stroke-width="0.4" fill="none"/>\n', dw-0.5, dw-0.5, fg_color);
      out += util.format('\t\t<circle cx="%d" cy="%d" r="3" stroke="%s" stroke-width="0.4" fill="none"/>\n', dw-0.5, dw-0.5, fg_color);
      out += util.format('\t\t<circle cx="%d" cy="%d" r="2" stroke="%s" stroke-width="0.4" fill="none"/>\n', dw-0.5, dw-0.5, fg_color);
    }
    out += util.format('\t\t<rect x="%d" y="%d" width="%d" height="%d" fill="%s"/>\n', dw-0.5, dw-0.5, symwidth+0.5, symwidth+0.5, bg_color);
    out += '\t\t<g id="Pattern">\n';

    for(y=0; y<width; y+=1) {
      pen = 0;
      x0 = 0;
      for(x=0; x<width; x+=1) {
        if(!pen) {
          pen = arr[y][x];
          x0 = x;
        } else {
          if(arr[y][x]===0) {
            out += genRect(x0+margin+dw, y+margin+dw, x-x0, fg_color);
            pen = 0;
          }
        }
      }
      if(pen) {
        out += genRect(x0+margin+dw, y+margin+dw, width-x0, fg_color);
      }
    }

    out += '\t\t</g>\n';
    out += '\t</g>\n';
    out += '</svg>';

    return out;
  }

  var qr = new qrencoder();
  qr.encode(text.toString(), {ec: eclevel}, function(err, arr) {
    if(!err) {
      callback(null, new Buffer(svgGenerator(arr, color_bg, color_fg)));
    } else {
      callback(true);
    }
  });
};

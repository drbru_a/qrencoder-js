qrencoder-js
------------

qrencoder -генерирует бинарную матрицу QR кода
var qr = new qrencoder();
qr.encode(text, {ec: eclevel}, function(err, arr) {
...
});

image_png - генерирует png изображение QR кода
var qrencoder = require('qrencoder-js');
qrencoder.image_png(req.query.mess, params, function(err, data) {
...
});
params:
    bg - цвет заднего фона (#rrggbb)
    fg - цвет переднего фона (#rrggbb)
    boxsize - размер элемента QR кода (в пикселях)
    margin - размер рамки вокруг QR кода (в элементах boxsize)
    eclevel - уровень коррекции ошибок (L, M, Q, H)


image_svg - генерирует svg изображение QR кода
var qrencoder = require('qrencoder-js');
qrencoder.image_svg(req.query.mess, params, function(err, data) {
...
});
params:
    bg - цвет заднего фона (#rrggbb)
    fg - цвет переднего фона (#rrggbb)
    boxsize - размер элемента QR кода (в пикселях)
    margin - размер рамки вокруг QR кода (в элементах boxsize)
    eclevel - уровень коррекции ошибок (L, M, Q, H)
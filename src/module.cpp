#include <node.h>

#include "QRencoder.h"

extern "C" void
init(v8::Handle<v8::Object> target)
{
    v8::HandleScope scope;

    QRencoder::Initialize(target);
}


/*
 * QRencoder.h
 *
 *  Created on: 17.04.2013
 *      Author: ba
 */

#ifndef QRENCODER_H_
#define QRENCODER_H_

#include <node.h>
#include <node_buffer.h>
#include "qrencode.h"

class QRencoder : public node::ObjectWrap {
    static void UV_QRencode(uv_work_t *req);
    static void UV_QRencodeAfter(uv_work_t *req);
public:
    static void Initialize(v8::Handle<v8::Object> target);
    QRencoder();

    static v8::Handle<v8::Value> New(const v8::Arguments &args);
    static v8::Handle<v8::Value> QRencodeAsync(const v8::Arguments &args);
};

#endif /* QRENCODER_H_ */

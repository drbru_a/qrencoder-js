/*
 * QRencoder.cpp
 *
 *  Created on: 17.04.2013
 *      Author: ba
 */

#include <cstring>
#include <cstdlib>
#include <unistd.h>
#include <cassert>
#include "QRencoder.h"

using namespace v8;
using namespace node;

Handle<Value> ErrorException(const char *msg)
{
    HandleScope scope;
    return Exception::Error(String::New(msg));
}

Handle<Value> VException(const char *msg) {
    HandleScope scope;
    return ThrowException(ErrorException(msg));
}

bool str_eq(const char *s1, const char *s2)
{
    return strcmp(s1, s2) == 0;
}

struct encode_request {
    v8::Persistent<v8::Function> callback;
    void *qr_obj; //
    char *str; //строка сообщения
	int version; //версия 0-выбирается автоматически (1-40)
	QRecLevel eclevel; //уровень коррекции ошибок
	QRencodeMode mode; //режим кодирования
	int casesensitive; //учет регистра символов (1-есть учет, 0-нет учета)
    char *data; //массив матрицы qr кода (размер width*width)
    int width; //ширина qr кода
    char *error; //ошибка
};

void QRencoder::Initialize(Handle<Object> target)
{
    HandleScope scope;

    Local<FunctionTemplate> t = FunctionTemplate::New(New);
    t->InstanceTemplate()->SetInternalFieldCount(1);
    NODE_SET_PROTOTYPE_METHOD(t, "encode", QRencodeAsync);
    target->Set(String::NewSymbol("qr"), t->GetFunction());
    //target = t->GetFunction();
}

QRencoder::QRencoder()
{
}


Handle<Value> QRencoder::New(const Arguments &args)
{
    HandleScope scope;

    QRencoder *_qrencoder = new QRencoder;
    _qrencoder->Wrap(args.This());

    return args.This();
}

void QRencoder::UV_QRencode(uv_work_t *req)
{
    encode_request *enc_req = (encode_request *)req->data;

    QRcode *code = QRcode_encodeString(enc_req->str, enc_req->version, enc_req->eclevel, enc_req->mode, enc_req->casesensitive);
    if(code) {
    	const int len = code->width*code->width;
    	enc_req->width = code->width;
    	enc_req->data = (char *)malloc(len);
    	for(int i=0; i<len; ++i) {
    		if(code->data[i] & 1) enc_req->data[i] = 1; else enc_req->data[i] = 0;
    	}
    } else {
    	enc_req->error = strdup("QRcode_encodeString failed.");
    }
}

void QRencoder::UV_QRencodeAfter(uv_work_t *req)
{
    HandleScope scope;

    encode_request *enc_req = (encode_request *)req->data;
    delete req;

    Handle<Value> argv[2];

    if (enc_req->error) {
    	argv[0] = ErrorException(enc_req->error);
        argv[1] = Undefined();
    }
    else {
        //Buffer *buf = Buffer::New(enc_req->png_len);
        //memcpy(BufferData(buf), enc_req->png, enc_req->png_len);
    	int w = enc_req->width;
    	Local<Array> buf = Array::New(w);
    	for(int y=0; y<w; ++y) {
    		Local<Array> buf1 = Array::New(w);
    		for(int x=0; x<w; ++x) {
    			buf1->Set(x, Number::New(enc_req->data[x+y*w]));
    		}
    		buf->Set(y, buf1);
    	}
    	argv[0] = Undefined();
        argv[1] = buf;
    }

    TryCatch try_catch; // don't quite see the necessity of this

    enc_req->callback->Call(Context::GetCurrent()->Global(), 2, argv);

    if (try_catch.HasCaught())
        FatalException(try_catch);

    enc_req->callback.Dispose();
    free(enc_req->data);
    free(enc_req->str);
    free(enc_req->error);

    ((QRencoder *)enc_req->qr_obj)->Unref();
    free(enc_req);
}

Handle<Value> QRencoder::QRencodeAsync(const Arguments &args)
{
    HandleScope scope;
    QRecLevel eclevel = QR_ECLEVEL_H;

    if (args.Length() != 2 && args.Length() != 3)
        return VException("Two or three argument required - string, [params], callback function.");

    if (!args[0]->IsString())
        return VException("First argument must be a string.");

    if(args.Length() == 2)
    {
    	if (!args[1]->IsFunction())
    		return VException("Second argument must be a function.");
    }
    else if(args.Length() == 3)
    {
    	if (!args[1]->IsObject())
    		return VException("Second argument must be a params.");

    	if (!args[2]->IsFunction())
    		return VException("Third argument must be a function.");

    	Local<Object> params = Local<Object>::Cast(args[1]);
    	if( params->Has(String::New("ec")) )
    	{
    		String::Utf8Value ecvalue( params->Get(String::New("ec"))->ToString() );
    		if(str_eq("H", *ecvalue))
    			eclevel = QR_ECLEVEL_H;
    		else if(str_eq("Q", *ecvalue))
    			eclevel = QR_ECLEVEL_Q;
    		else if(str_eq("M", *ecvalue))
    			eclevel = QR_ECLEVEL_M;
    		else if(str_eq("L", *ecvalue))
    			eclevel = QR_ECLEVEL_L;
    	}
    }

    String::Utf8Value str(args[0]);
    Local<Function> callback = Local<Function>::Cast(args[args.Length()-1]);
    QRencoder *_qrencoder = ObjectWrap::Unwrap<QRencoder>(args.This());

    encode_request *enc_req = (encode_request *)malloc(sizeof(*enc_req));
    if (!enc_req)
        return VException("malloc in QRencoder::QRencodeAsync failed.");

    enc_req->callback = Persistent<Function>::New(callback);
    enc_req->qr_obj = _qrencoder;
    enc_req->data = NULL;
    enc_req->width = 0;
    enc_req->error = NULL;

    enc_req->str = strdup(*str);
    enc_req->version = 0;
    enc_req->eclevel = eclevel;
    enc_req->mode = QR_MODE_8;
    enc_req->casesensitive = 1;

    uv_work_t* req = new uv_work_t;
    req->data = enc_req;
    uv_queue_work(uv_default_loop(), req, UV_QRencode, (uv_after_work_cb)UV_QRencodeAfter);

    _qrencoder->Ref();

    return Undefined();
}

NODE_MODULE(qrencoder, QRencoder::Initialize)